plot_cohorts <- function(cohort_data, plotly = FALSE, var = "pct_stop", plot_title = NULL) {
  
  plot = cohort_data %>% 
    ggplot() + geom_line(aes(x = cohort_age_weeks, 
                             y = .data[[var]], 
                             size = cohort_size/1000,
                             color = cohort_labels,
                             alpha = cohort_labels)) +
    ylab(var) +
    geom_line(data = cohort_data %>% group_by(cohort_age_weeks) %>% 
                summarize(mean_stat = mean(.data[[var]])),
              aes(x = cohort_age_weeks, y = mean_stat), lty = 3, show.legend = FALSE)
  
  if (!is.null(plot_title)) {plot = plot + ggtitle(plot_title)}
  if (plotly) {
    plot = plot + guides(size = "none")
    plot = ggplotly(plot)
  }
  
  return(plot)
}
